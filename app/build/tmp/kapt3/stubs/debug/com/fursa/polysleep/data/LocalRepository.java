package com.fursa.polysleep.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010J\u0012\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\u0010J\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\u0010J\u0012\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\u0010J\u0010\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u0014H\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0019"}, d2 = {"Lcom/fursa/polysleep/data/LocalRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "sleepDao", "Lcom/fursa/polysleep/data/SleepDao;", "getSleepDao", "()Lcom/fursa/polysleep/data/SleepDao;", "sleepDao$delegate", "Lkotlin/Lazy;", "cancelSleep", "", "id", "", "count", "Lio/reactivex/Flowable;", "", "getAll", "", "Lcom/fursa/polysleep/data/Sleep;", "getAllAsc", "getAllDesc", "save", "sleep", "app_debug"})
public final class LocalRepository {
    private final kotlin.Lazy sleepDao$delegate = null;
    private final android.content.Context context = null;
    
    private final com.fursa.polysleep.data.SleepDao getSleepDao() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"CheckResult"})
    public final void save(@org.jetbrains.annotations.NotNull()
    com.fursa.polysleep.data.Sleep sleep) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Flowable<java.util.List<com.fursa.polysleep.data.Sleep>> getAllAsc() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Flowable<java.util.List<com.fursa.polysleep.data.Sleep>> getAll() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Flowable<java.util.List<com.fursa.polysleep.data.Sleep>> getAllDesc() {
        return null;
    }
    
    public final void cancelSleep(long id) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.reactivex.Flowable<java.lang.Integer> count() {
        return null;
    }
    
    public LocalRepository(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
}