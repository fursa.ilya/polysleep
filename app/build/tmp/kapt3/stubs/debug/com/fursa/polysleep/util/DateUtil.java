package com.fursa.polysleep.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000*\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0000\u001a\u0016\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\f\u001a\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000f\u001a\u00020\u0012\"\u001b\u0010\u0000\u001a\u00020\u00018FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0002\u0010\u0003\"\u001b\u0010\u0006\u001a\u00020\u00078FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\n\u0010\u0005\u001a\u0004\b\b\u0010\t\u00a8\u0006\u0013"}, d2 = {"calendar", "Ljava/util/Calendar;", "getCalendar", "()Ljava/util/Calendar;", "calendar$delegate", "Lkotlin/Lazy;", "sdf", "Ljava/text/SimpleDateFormat;", "getSdf", "()Ljava/text/SimpleDateFormat;", "sdf$delegate", "addMinutes", "Ljava/util/Date;", "minutes", "", "currentTime", "dateToString", "", "", "app_debug"})
public final class DateUtil {
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Lazy sdf$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private static final kotlin.Lazy calendar$delegate = null;
    
    @org.jetbrains.annotations.NotNull()
    public static final java.text.SimpleDateFormat getSdf() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.Calendar getCalendar() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.util.Date addMinutes(int minutes, @org.jetbrains.annotations.NotNull()
    java.util.Date currentTime) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String dateToString(long currentTime) {
        return null;
    }
}