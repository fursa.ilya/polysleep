package com.fursa.polysleep;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"}, d2 = {"EIGHT_HOURS", "", "THIRTY_MINUTES", "TWENTY_MINUTES", "app_debug"})
public final class MainActivityKt {
    public static final int TWENTY_MINUTES = 20;
    public static final int THIRTY_MINUTES = 30;
    public static final int EIGHT_HOURS = 480;
}