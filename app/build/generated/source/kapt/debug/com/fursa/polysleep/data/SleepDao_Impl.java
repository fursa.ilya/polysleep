package com.fursa.polysleep.data;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.RxRoom;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import io.reactivex.Flowable;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings("unchecked")
public final class SleepDao_Impl implements SleepDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfSleep;

  private final SharedSQLiteStatement __preparedStmtOfCancelSleep;

  public SleepDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfSleep = new EntityInsertionAdapter<Sleep>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Sleep`(`taskId`,`start_time`,`end_time`,`finished`) VALUES (?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Sleep value) {
        stmt.bindLong(1, value.getTaskId());
        stmt.bindLong(2, value.getStartTime());
        stmt.bindLong(3, value.getEndTime());
        final int _tmp;
        _tmp = value.isFinished() ? 1 : 0;
        stmt.bindLong(4, _tmp);
      }
    };
    this.__preparedStmtOfCancelSleep = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM Sleep WHERE taskId=?";
        return _query;
      }
    };
  }

  @Override
  public void save(Sleep sleep) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfSleep.insert(sleep);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void cancelSleep(long id) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfCancelSleep.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      _stmt.bindLong(_argIndex, id);
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfCancelSleep.release(_stmt);
    }
  }

  @Override
  public Flowable<List<Sleep>> getAllDesc() {
    final String _sql = "SELECT * FROM Sleep ORDER BY start_time DESC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"Sleep"}, new Callable<List<Sleep>>() {
      @Override
      public List<Sleep> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
          final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
          final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
          final int _cursorIndexOfIsFinished = _cursor.getColumnIndexOrThrow("finished");
          final List<Sleep> _result = new ArrayList<Sleep>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Sleep _item;
            final long _tmpTaskId;
            _tmpTaskId = _cursor.getLong(_cursorIndexOfTaskId);
            final long _tmpStartTime;
            _tmpStartTime = _cursor.getLong(_cursorIndexOfStartTime);
            final long _tmpEndTime;
            _tmpEndTime = _cursor.getLong(_cursorIndexOfEndTime);
            final boolean _tmpIsFinished;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsFinished);
            _tmpIsFinished = _tmp != 0;
            _item = new Sleep(_tmpTaskId,_tmpStartTime,_tmpEndTime,_tmpIsFinished);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<List<Sleep>> getAllAsc() {
    final String _sql = "SELECT * FROM Sleep ORDER BY start_time ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"Sleep"}, new Callable<List<Sleep>>() {
      @Override
      public List<Sleep> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
          final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
          final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
          final int _cursorIndexOfIsFinished = _cursor.getColumnIndexOrThrow("finished");
          final List<Sleep> _result = new ArrayList<Sleep>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Sleep _item;
            final long _tmpTaskId;
            _tmpTaskId = _cursor.getLong(_cursorIndexOfTaskId);
            final long _tmpStartTime;
            _tmpStartTime = _cursor.getLong(_cursorIndexOfStartTime);
            final long _tmpEndTime;
            _tmpEndTime = _cursor.getLong(_cursorIndexOfEndTime);
            final boolean _tmpIsFinished;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsFinished);
            _tmpIsFinished = _tmp != 0;
            _item = new Sleep(_tmpTaskId,_tmpStartTime,_tmpEndTime,_tmpIsFinished);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<List<Sleep>> getAll() {
    final String _sql = "SELECT * FROM Sleep LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"Sleep"}, new Callable<List<Sleep>>() {
      @Override
      public List<Sleep> call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfTaskId = _cursor.getColumnIndexOrThrow("taskId");
          final int _cursorIndexOfStartTime = _cursor.getColumnIndexOrThrow("start_time");
          final int _cursorIndexOfEndTime = _cursor.getColumnIndexOrThrow("end_time");
          final int _cursorIndexOfIsFinished = _cursor.getColumnIndexOrThrow("finished");
          final List<Sleep> _result = new ArrayList<Sleep>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Sleep _item;
            final long _tmpTaskId;
            _tmpTaskId = _cursor.getLong(_cursorIndexOfTaskId);
            final long _tmpStartTime;
            _tmpStartTime = _cursor.getLong(_cursorIndexOfStartTime);
            final long _tmpEndTime;
            _tmpEndTime = _cursor.getLong(_cursorIndexOfEndTime);
            final boolean _tmpIsFinished;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsFinished);
            _tmpIsFinished = _tmp != 0;
            _item = new Sleep(_tmpTaskId,_tmpStartTime,_tmpEndTime,_tmpIsFinished);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<Integer> count() {
    final String _sql = "SELECT COUNT(*) FROM Sleep";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return RxRoom.createFlowable(__db, new String[]{"Sleep"}, new Callable<Integer>() {
      @Override
      public Integer call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            final Integer _tmp;
            if (_cursor.isNull(0)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(0);
            }
            _result = _tmp;
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
