package com.fursa.polysleep.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import com.fursa.polysleep.MainActivity
import com.fursa.polysleep.R

const val CHANNEL_ID = "com.fursa.polysleep.alarm"
const val CHANNEL_NAME = "PolySleepChannel"

@RequiresApi(Build.VERSION_CODES.O)
class NotificationHelper(context: Context) : ContextWrapper(context) {

    private val vibrationPatternArr by lazy {
        longArrayOf(100, 200, 300, 400, 500, 400, 500, 200, 500)
    }

    private val mainActivityIntent by lazy {
        Intent(this@NotificationHelper, MainActivity::class.java)
    }


    val notificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE)
        as NotificationManager
    }

    init {
        val notificationChannel = NotificationChannel(
            CHANNEL_ID,
            CHANNEL_NAME,
            NotificationManager.IMPORTANCE_HIGH
        )

        notificationChannel.enableLights(true)
        notificationChannel.enableVibration(true)
        notificationChannel.vibrationPattern = vibrationPatternArr
        notificationManager.createNotificationChannel(notificationChannel)
    }

    companion object {
        fun getNotificationBuilder(title: String, content: String, context: Context): Notification.Builder {
            val mainActivityIntent = Intent(context, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, mainActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            return Notification.Builder(
                context.applicationContext, CHANNEL_ID
            ).setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_twotone_notifications_active_24px)
                .setAutoCancel(true)
        }
    }
}