package com.fursa.polysleep

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.fursa.polysleep.background.NotifWorker
import com.fursa.polysleep.data.LocalRepository
import com.fursa.polysleep.data.Sleep
import com.fursa.polysleep.util.addMinutes
import com.fursa.polysleep.util.dateToString
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit

const val TWENTY_MINUTES = 20
const val THIRTY_MINUTES = 30
const val EIGHT_HOURS = 480

class MainActivity : AppCompatActivity() {
    private lateinit var rootView: ConstraintLayout
    private lateinit var btnCancel: Button
    private lateinit var textViewStartTime: TextView
    private lateinit var textViewEndTime: TextView

    private val localRepository by lazy {
        LocalRepository(this@MainActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        rootView = findViewById(R.id.currentSleepLayout)
        btnCancel = rootView.findViewById(R.id.btnCancel)
        textViewStartTime = rootView.findViewById(R.id.startTime)
        textViewEndTime = rootView.findViewById(R.id.endTime)

        btnCancel.setOnClickListener { Toast.makeText(baseContext, "Cancel", Toast.LENGTH_LONG).show() }

        localRepository.count()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if(it > 0) {
                    currentSleepLayout.visibility = View.VISIBLE
                    showCurrentSleepInfo()
                } else {
                    currentSleepLayout.visibility = View.GONE
                }
            }



        cardViewThirtyMin.setOnClickListener {
            val calendar = addMinutes(THIRTY_MINUTES, Date())
            Toast.makeText(baseContext, "Будильник установлен на ${dateToString(calendar.time)}", Toast.LENGTH_LONG).show()
            enqueueNotification(calendar)
        }

        cardViewTwentyMin.setOnClickListener {
            val calendar = addMinutes(TWENTY_MINUTES, Date())
            Toast.makeText(baseContext, "Будильник установлен на ${dateToString(calendar.time)}", Toast.LENGTH_LONG).show()
            enqueueNotification(calendar)

        }

        cardViewFull.setOnClickListener {
            val calendar = addMinutes(EIGHT_HOURS, Date())
            Toast.makeText(baseContext, "Будильник установлен на ${dateToString(calendar.time)}", Toast.LENGTH_LONG).show()
            enqueueNotification(calendar)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when(menuItem.itemId) {
            R.id.settings_item -> { Toast.makeText(baseContext, getString(R.string.settings), Toast.LENGTH_LONG).show() }
        }
        return true
    }

    private fun showCurrentSleepInfo() {
        localRepository.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                startTime.text = dateToString(it[0].startTime)
                endTime.text = dateToString(it[0].endTime)
            }
    }

    private fun enqueueNotification(currentTime: Date) {
        val workRequest = OneTimeWorkRequest.Builder(NotifWorker::class.java)
            .setInitialDelay(currentTime.time - System.currentTimeMillis(), TimeUnit.MILLISECONDS)
            .build()
        WorkManager.getInstance().enqueue(workRequest)

        localRepository.save(
            Sleep(
                System.currentTimeMillis(),
                System.currentTimeMillis(),
                currentTime.time - System.currentTimeMillis(),
                false)
        )
    }
}
