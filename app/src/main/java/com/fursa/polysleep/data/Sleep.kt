package com.fursa.polysleep.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Sleep")
data class Sleep(
    @PrimaryKey
    @ColumnInfo(name = "taskId")
    val taskId: Long,

    @ColumnInfo(name = "start_time")
    val startTime: Long,

    @ColumnInfo(name = "end_time")
    val endTime: Long,

    @ColumnInfo(name = "finished")
    val isFinished: Boolean
)