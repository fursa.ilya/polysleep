package com.fursa.polysleep.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

const val DB_NAME = "sleep_db"

@Database(entities = [Sleep::class],
    version = 1,
    exportSchema = false
)
abstract class InnerDb: RoomDatabase() {

    companion object {
        private lateinit var LOCAL_DB_INSTANCE: InnerDb

        @Synchronized fun getInnerDb(c: Context, inMemory: Boolean): InnerDb {
            LOCAL_DB_INSTANCE = if(inMemory) {
                Room.inMemoryDatabaseBuilder(c, InnerDb::class.java)
                    .fallbackToDestructiveMigration()
                    .build()
            } else {
                Room.databaseBuilder(c.applicationContext, InnerDb::class.java, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()
            }

            return LOCAL_DB_INSTANCE
        }
    }

    abstract fun getSleepDao(): SleepDao
}