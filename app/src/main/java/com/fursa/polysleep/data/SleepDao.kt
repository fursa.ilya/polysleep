package com.fursa.polysleep.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import org.intellij.lang.annotations.Flow

@Dao
interface SleepDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(sleep: Sleep)

    @Query("SELECT * FROM Sleep ORDER BY start_time DESC")
    fun getAllDesc(): Flowable<List<Sleep>>

    @Query("SELECT * FROM Sleep ORDER BY start_time ASC")
    fun getAllAsc(): Flowable<List<Sleep>>

    @Query("SELECT * FROM Sleep LIMIT 1")
    fun getAll(): Flowable<List<Sleep>>

    @Query("DELETE FROM Sleep WHERE taskId=:id")
    fun cancelSleep(id: Long)

    @Query("SELECT COUNT(*) FROM Sleep")
    fun count(): Flowable<Int>
}