package com.fursa.polysleep.data

import android.annotation.SuppressLint
import android.content.Context
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

//Todo отписывать Rx

class LocalRepository(private val context: Context) {

    private val sleepDao by lazy {
        val db = InnerDb.getInnerDb(context, false)
        db.getSleepDao()
    }

     @SuppressLint("CheckResult")
     fun save(sleep: Sleep) {
         Flowable.just(sleep)
             .subscribeOn(Schedulers.io())
             .subscribe { sleep -> sleepDao.save(sleep) }
     }

    fun getAllAsc(): Flowable<List<Sleep>> {
        return sleepDao.getAllAsc()
    }

    fun getAll(): Flowable<List<Sleep>> {
        return sleepDao.getAll()
    }

    fun getAllDesc(): Flowable<List<Sleep>> {
        return sleepDao.getAllDesc()
    }

    fun cancelSleep(id: Long) {
        sleepDao.cancelSleep(id)
    }

    fun count(): Flowable<Int> {
        return sleepDao.count()
    }

}