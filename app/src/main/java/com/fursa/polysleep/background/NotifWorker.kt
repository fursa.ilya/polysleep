package com.fursa.polysleep.background

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.fursa.polysleep.R
import com.fursa.polysleep.notification.NotificationHelper

@RequiresApi(Build.VERSION_CODES.O)
class NotifWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {

    private val notificationHelper by lazy {
        NotificationHelper(context)
    }

    override fun doWork(): Result {
        showNotification(
            applicationContext.getString(R.string.app_name),
            applicationContext.getString(R.string.app_slogan)
        )
        return Result.success()
    }

    private fun showNotification(title: String, content: String) {
        val builder = NotificationHelper.getNotificationBuilder(title, content, applicationContext)
        notificationHelper.notificationManager.notify(0, builder.build())
    }
}