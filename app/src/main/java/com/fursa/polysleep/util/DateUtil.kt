@file:JvmName("DateUtil")
package com.fursa.polysleep.util

import java.text.SimpleDateFormat
import java.util.*


val sdf by lazy {
    SimpleDateFormat("HH:mm")
}

val calendar: Calendar by lazy {
    Calendar.getInstance()
}

fun addMinutes(minutes: Int, currentTime: Date): Date {
    calendar.time = currentTime
    calendar.add(Calendar.MINUTE, minutes)
    return calendar.time
}

fun dateToString(currentTime: Long): String {
   val date = Date(currentTime)
   return sdf.format(date)
}